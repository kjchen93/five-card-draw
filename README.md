# Task: 1.4 Poker Hands

## About

**Author**: Kai Chen 


**Description**: A code that draws 5 cards from 2 shuffled deck of cards and
displays the type of hand the player has. The code evaluate the strongest hand and prints out in following manner: 


Sample of a run



![](./sample_output.jpg)


### Conventions used:
- A = Ace
- K = King
- Q = Queen
- J = Jack
- T = Ten



- D = Diamonds
- C = Clubs
- H = Hearts
- S = Spades

## Usage

Go to /src directory 
```sh
cd ~/src
```
compile 

```sh
 g++ fivecards.cpp -o fivecards

```
run 
```sh
 ./fivecards
```





