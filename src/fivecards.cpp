#include<iostream>
#include<string>
#include<vector>
#include<random>
#include<algorithm>
#include<chrono>
using namespace std;

/* 
Author: Kai Chen
Description: A code that draws 5 cards from 2 shuffled deck of cards and
displays the type of hand the player has. 

conventions used:
A = Ace
K = King
Q = Queen
J = Jack
T = Ten

D = Diamonds
C = Clubs
H = Hearts
S = Spades

=================================================
Uncomment and add to main() to test the functions
-------------------------------------------------
    vector<string> somee = {"8H","KC","4D","3D","AD"};

    cout << "is 5ofK : " << isFiveOfAKind(somee) << endl;
    cout << "is flush : " << isFlush(somee) << endl;
    cout << "is straight : " << isStraight(somee) << endl;
    cout << "is straightflush : " << isStraightFlush(somee) << endl;
    cout << "is 4oK : " << isFourOfAKind(somee) << endl;
    cout << "is fullhouse : " << isFullHouse(somee) << endl;
    cout << "is 3oK : " << isThreeOfAKind(somee) << endl;
    cout << "is 2pair : " << isTwoPair(somee) << endl;
    cout << "is 1pair : " << isOnePair(somee) << endl;
    cout << "is HighCard : " << isHighCard(somee) << endl;
=================================================
*/

vector<string> makeDeck();
bool isFiveOfAKind(vector<string> hand);
bool isStraightFlush(vector<string> hand);
bool isFourOfAKind(vector<string> hand);
bool isFullHouse(vector<string> hand);
bool isFlush(vector<string> hand);
bool isStraight(vector<string> hand);
bool isThreeOfAKind(vector<string> hand);
bool isTwoPair(vector<string> hand);
bool isOnePair(vector<string> hand);
bool isHighCard(vector<string> hand);
vector<int> getHandRanks(vector<string> hand);

int main() {
    vector<string> deck1, deck2, hand;
    vector<char> hand_suits;
    vector<int> hand_ranks;
    string card_drawn;

    // Create 2 decks of cards and merges into 1 large deck
    deck1 = makeDeck();
    deck2 = makeDeck();
    vector<string> deck_total(deck1);
    deck_total.insert(deck_total.end(), deck2.begin(), deck2.end());

    // Shuffle the large deck
    unsigned seed = chrono::system_clock::now()
                        .time_since_epoch()
                        .count();
    shuffle(begin(deck_total), end(deck_total), default_random_engine(seed));
    
    //Draw 5 cards to get a hand
    for(int i = 0;  i <= 5; ++i){
        card_drawn = deck_total.back();
        deck_total.pop_back();
        hand.push_back(card_drawn);
    }

    // Prints the hand
    for (int icard = 0; icard < 5; icard++) 
    {
        for (int k = 0; k < 9; ++k){
            for (int l = 0; l < 9; ++l){
                if( (k == 0) || (k == 9-1)) {
                    cout << "-"; // prints a row of -- in the first and last row
                }else if ((k != 0) && ((l==0)|| (l== 9-1))){
                    cout << "|"; // first and last column of a middle row is a |
                }else if ((k==4) && (l==4)){
                    cout << hand[icard];
                }else if(((k==4) && (l==3))){
                    cout << ""; 
                }else{
                    cout << " ";
                }
        
        }
        cout << "\n"; // new row
    }
    }
    
    // Check what type of hand it is
    if(isFiveOfAKind(hand)){
        cout << "You've got 5 of a kind!" << endl;
    }else if (isStraightFlush(hand))
    {
        cout << "You've got straight flush!" <<endl;
    }else if (isFourOfAKind(hand))
    {
        cout << "You've got four of a kind!" <<endl;
    }else if (isFullHouse(hand))
    {
        cout << "You've got full house!" <<endl;
    }else if (isFlush(hand))
    {
        cout << "You've got flush!" <<endl;
    }else if (isStraight(hand))
    {
        cout << "You've got straight!" <<endl;
    }else if (isThreeOfAKind(hand))
    {
        cout << "You've got three of a kind!" <<endl;
    }else if (isTwoPair(hand))
    {
        cout << "You've got two pair!" <<endl;
    }else if (isOnePair(hand))
    {
        cout << "You've got one pair!" <<endl;
    }else{
        cout << "You've got high card!" <<endl; // if none of the above is true then it must be high card
    }
    
    return 0;
}



// Function that creates a deck 
vector<string> makeDeck(){
    vector<string> deck;
    string suit, rank;

    for (int i = 0; i < 4; ++i){
        for (int j = 1; j <= 13; ++j){
            if(i == 0){
                suit = "D";
            }
            if (i == 1)
            {
                suit = "C"; 
            }
            if (i == 2)
            {
                suit = "H"; 
            }
            if (i == 3)
            {
                suit = "S"; 
            }
            if(j == 1){
                rank = "A";
            }else if (j == 10)
            {
                rank = "T";
            }
            else if (j == 11)
            {
                rank = "J";
            }else if (j == 12)
            {
                rank = "Q";
            }else if (j == 13)
            {
                rank = "K";
            }else{
                rank = to_string(j);
            }

            deck.push_back(rank + suit);
        }
    }
    return deck;
}




bool isFiveOfAKind(vector<string> hand){
    for (int i = 1; i < 5; i++)
    {
        if(hand[0][0] != hand[i][0]){
            return false; //returns false when one of the ranks differs from the other cards
        }
    }
    return true;
}


bool isFlush(vector<string> hand){
    for (int i = 1; i < 5; i++)
    {
        if(hand[0][1] != hand[i][1]){
            return false; //returns false if one of the suits differs
        }
    }
    return true;
}

bool isStraight(vector<string> hand){
    vector<int> ranks;
    ranks = getHandRanks(hand); //gets ranks in integers
    
    sort(ranks.begin(), ranks.end()); // sort smallest to biggest
    if (ranks[0] == 2 && ranks[4] == 14) // accounts for the case A2345
    {
        ranks[4] = 1;
    }
    
    for(int j = 0; j < 4; ++j){
        if((ranks[j+1] - ranks[j] > 1) || (ranks[j+1] - ranks[j] == 0)){
            return false; // increments in straight must be exactly one
        }
    }
    return true;
}

bool isStraightFlush(vector<string> hand){
    if ((isStraight(hand)) && (isFlush(hand))){
        return true;
    }
    return false;
}

bool isFourOfAKind(vector<string> hand){
    vector<int> ranks;
    ranks = getHandRanks(hand);

    for (int i = 0; i < 2; i++)
    {
        int same_ranks = count(ranks.begin(), ranks.end(), ranks[i]);
        if (same_ranks == 4){
            return true; // true if four of the ranks are the same
        }
    }
    return false;
}

bool isFullHouse(vector<string> hand){
    vector<int> ranks;
    ranks = getHandRanks(hand);
    sort(ranks.begin(), ranks.end());
    int same_rank_small = count(ranks.begin(), ranks.end(), ranks[0]);
    int same_rank_big = count(ranks.begin(), ranks.end(), ranks[4]);
    // after sort its either AAABB or BBAAA
    if (((same_rank_small == 2) && (same_rank_big == 3))){
        return true;
    }else if (((same_rank_small == 3) && (same_rank_big == 2)))
    {
        return true;
    }
    return false;
}

bool isThreeOfAKind(vector<string> hand){
    vector<int> ranks;
    ranks = getHandRanks(hand);
    for (int i = 0; i < 3; i++)
    {
        int same_ranks = count(ranks.begin(), ranks.end(), ranks[i]);
        if (same_ranks == 3){
            return true;
        }
    }
    return false;
}

bool isTwoPair(vector<string> hand){
    vector<int> ranks;
    ranks = getHandRanks(hand);
    sort(ranks.begin(), ranks.end());
    int same_rank_small = count(ranks.begin(), ranks.end(), ranks[0]);
    int same_rank_middle = count(ranks.begin(), ranks.end(), ranks[0]);
    int same_rank_big = count(ranks.begin(), ranks.end(), ranks[4]);
    // after sort it is either AABCC or ABBCC or AABBC
    if (((same_rank_small == 2) && (same_rank_big == 2))){
        return true;
    }else if (((same_rank_small == 2) && (same_rank_middle == 2)))
    {
        return true;
    }else if (((same_rank_middle == 2) && (same_rank_big == 2)))
    {
        return true;
    }
    return false;
}

bool isOnePair(vector<string> hand){
    vector<int> ranks;
    ranks = getHandRanks(hand);
    for (int i = 0; i < 4; i++)
    {
        int same_ranks = count(ranks.begin(), ranks.end(), ranks[i]);
        if (same_ranks == 2){
            return true;
        }
    }
    return false;
}


vector<int> getHandRanks(vector<string> hand){
    vector<int> ranks;
    for (int i = 0; i < 5; i++){
        if(hand[i][0] == 'A')
        {
            ranks.push_back(14);
        }else if (hand[i][0] == 'K')
        {
            ranks.push_back(13);
        }else if (hand[i][0] == 'Q')
        {
            ranks.push_back(12);
        }else if (hand[i][0] == 'J')
        {
            ranks.push_back(11);
        }else if (hand[i][0] == 'T')
        {
            ranks.push_back(10);
        }else{
            ranks.push_back((int)hand[i][0] - 48); //The 48 is  ASCII code correction
        }
    }
    return ranks;
}